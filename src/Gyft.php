<?php namespace Gyft;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Exception\ClientException;

class Gyft
{
	protected static $environment = [
		'sandbox'    => 'https://apitest.gyft.com/mashery/v1/reseller/',
		'production' => 'https://api.gyft.com/mashery/v1/reseller/'
	];

	private $client;

	private $api_key;

	private $secret;

	/**
	 * @param string $api_key
	 * @param string $secret
	 * @param string $env Environment name, can be "sanbox" (default) or "production"
	 */
	public function __construct($api_key, $secret, $env = 'sandbox')
	{
		$this->api_key = $api_key;

		$this->secret = $secret;

		// Detect API base url
		$config['baseUrl'] = in_array($env, self::$environment)
			? self::$environment[$env]
			: self::$environment['sandbox'];

		$config = array_merge($config, json_decode(file_get_contents(__DIR__.'/client.json'), TRUE));

		$this->client = new GuzzleClient(new Client, new Description($config));
	}

	public function getShopCards()
	{
		return $this->run('getShopCards');
	}

	public function purchaseCard($shop_card_id, $to_email, $params = [])
	{
		$params['shop_card_id'] = $shop_card_id;
		$params['to_email']     = $to_email;

		return $this->run('purchaseCard', $params);
	}

	public function run($function, $params = [])
	{
		$timestamp = time();

		$params = array_merge([
			'api_key'         => $this->api_key, 
			'sig'             => $this->signature($timestamp),
			'x-sig-timestamp' => $timestamp
		], $params);

		return $this->client->$function($params);
	}

	/**
	 * Method returns API request signature
	 * 
	 * @param integer $timestamp
	 * @return string
	 */
	private function signature($timestamp)
	{
		return bin2hex(hash('sha256', $this->api_key . $this->secret . $timestamp, TRUE));
	}
}